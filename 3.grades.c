/* from http://www.tutorialspoint.com/gnu_debugger/gdb_debugging_example2.htm */
#include <stdio.h>
#include <string.h>

int histogram[10];
int count = 0;

void add_to_histogram(int grade) {
	if (grade < 0 || grade > 100) {
		fprintf(stderr, "wrong input: %d\n", grade);
		return;
	}
	histogram[grade/10] += 1;
}

void print_histogram(int value) {
	int j;

	for (j = 0; j < value; ++j)
		putchar('*');
	putchar('\n');
}

int main() {
	int i;
	int grade = 0;

	for (count = 0; scanf("%d", &grade) == 1; ++count) {
		add_to_histogram(grade);
	}

	printf("There are totally %d grades.\n", count);
	for (i = 0; i < 10; ++i) {
		printf("%-2d - %-2d: ", i * 10, i * 10 + 9);
		print_histogram(histogram[i]);
	}

	count = -1;
	histogram[count] = 0;

	return 0;
}
