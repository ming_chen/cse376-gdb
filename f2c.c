/*
 * f2c.c:
 * Convert temperatures from Fahrenheit to Celsius.
 * Erez Zadok, Fall 2014.
 */

#include <stdio.h>

/*
 * convert():
 * Takes a floating number as input (Fahrenheit degrees).
 * Returns a floating number as output (Celsius degrees).
 */
float convert(float f)
{
	float c;

	c = (f - 32) * 5.0 / 9.0;
	return c;
}

/*
 * main():
 * Iterate over a range of numbers, convert them from Fahrenheit to
 * Celsius degrees, and print them.
 */
int main()
{
	float min, max, step, f, c;

	min = -40.0;
	max = 100.0;
	step = 25.0;

	for (f = min; f <= max; f += step) {
		c = convert(f);
		printf("%6.2ff = %6.2fc\n", f, c);
	}

	return 0;
}
