/* from http://www.cprogramming.com/gdb.html */

#include <stdio.h>
 
long factorial(int n)
{
	long result = 1;
	while (n) {
		result *= n--;
	}
	return result;
}

int main()
{
	int n;
	scanf("%d", &n);
	long val = factorial(n);
	printf("factorial(%d) = %ld\n", n, val);
	return 0;
}
