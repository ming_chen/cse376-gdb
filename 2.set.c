/* http://www.tutorialspoint.com/gnu_debugger/gdb_debugging_example2.htm */
#include <stdio.h> 

void setint(int* ip, int i) {
	*ip = i;
} 

int main() {
	int a;
	setint(&a, 10);
	printf("a = %d\n", a);

	int b;
	int* pb = &b;
	setint(pb, 20);
	printf("b = %d\n", b);
	return 0;
}
